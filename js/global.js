//SOUNDS
var audio = [];

//TIMEOUTS
var timeout = [];

//AWARDs NUMBER
var awardNum = 0,
	globalName, 
	launch = [],
	typingText = [];

var allHaveHtml = function(jqueryElement){
	for(var i = 0; i < jqueryElement.length; i ++)
	{
		if(!$(jqueryElement[i]).html())
			return false;
	}
	return true;
}

var fadeOneByOne = function(jqueryElement, curr, interval, endFunction)
{
	if (curr < jqueryElement.length)
	{
		timeout[curr] = setTimeout(function(){
			$(jqueryElement[curr]).fadeIn(200);
			fadeOneByOne(jqueryElement, ++curr, interval, endFunction);
		}, interval);
	}
	else
		endFunction();
}

function TypingText(jqueryElement, interval, hasSound, endFunction)
{
	this.text = jqueryElement.html();
	this.jqueryElement = jqueryElement;
	this.interval = interval;
	this.currentSymbol = 0;
	this.endFunction = endFunction;
	this.hasSound = hasSound;
	this.audio = new Audio("audio/keyboard-sound.mp3");
	this.audio.addEventListener("ended", function(){
			this.play();
		});
	
	this.timeout;
	this.jqueryElement.html("");
}

TypingText.prototype.write = function()
{
	tObj = this;
	if (tObj.audio.paused && tObj.hasSound) {
		tObj.audio.play();
	}
	tObj.timeout = setTimeout(function(){
		if (tObj.text[tObj.currentSymbol] === "/" && tObj.text[tObj.currentSymbol + 1] === 'n')
		{
			tObj.jqueryElement.append("<br>");
			tObj.currentSymbol += 2;
		}
		else if (tObj.text[tObj.currentSymbol] === "/" && tObj.text[tObj.currentSymbol + 1] === 'g')
		{
			tObj.currentSymbol += 2;
			tObj.jqueryElement.append("<b class='lightgreen'>" + tObj.text[tObj.currentSymbol] + "</b>");
			tObj.currentSymbol ++;
		}
		else if (tObj.text[tObj.currentSymbol] === "/" && tObj.text[tObj.currentSymbol + 1] === 'b')
		{
			tObj.currentSymbol += 2;
			tObj.jqueryElement.append("<b class='lightblue'>" + tObj.text[tObj.currentSymbol] + "</b>");
			tObj.currentSymbol ++;
		}
		else
		{
			tObj.jqueryElement.append(tObj.text[tObj.currentSymbol]);
			tObj.currentSymbol ++;
		}
		if (tObj.currentSymbol < tObj.text.length) 
			tObj.write();
		else
		{
			tObj.audio.pause();
			tObj.endFunction();
		}
	}, tObj.interval);
}

TypingText.prototype.stopWriting = function()
{
	clearTimeout(this.timeout);
	this.audio.pause();
}

var loadImages = function(){
	jQuery.get('fileNames.txt', function(data) {
		var imagesSrc = data.split("\n"),
			images = [],
			loadPercentage = 0,
			imagesNum = imagesSrc.length - 1;
			unitToAdd = Math.round(100 / imagesNum);
		
		var imageLoadListener = function(){
			loadPercentage += unitToAdd;
			console.log(loadPercentage);
			imagesNum --;
			if (loadPercentage >= 100) {
				//hideEverythingBut($("#frame-000"));
			}
		};
		
		for (var i = 0; i < imagesSrc.length - 1; i ++)
		{
			images[i] = new Image();
			images[i].src = "pics/" + imagesSrc[i];
			images[i].addEventListener("load", imageLoadListener);
		}
	});
}

function DragTask(jqueryElements, successCondition, successFunction, failFunction, finishCondition, finishFunction)
{
	this.draggables = jqueryElements;
	this.draggabillies = [];
	this.vegetable;
	this.basket;
	
	this.makeThemDraggable = function()
	{
		for(var i = 0; i < this.draggables.length; i++)
			this.draggabillies[i] = new Draggabilly(this.draggables[i]);
	}
	
	this.addEventListeners = function()
	{
		for(var i = 0; i < this.draggabillies.length; i++)
		{
			this.draggabillies[i].on("dragStart", this.onStart);
			this.draggabillies[i].on("dragEnd", this.onEnd);
		}
	}
	
	this.onEnd = function(instance, event, pointer)
	{
		var currVeg = this.vegetable;
		var currBasket = this.basket;
		currVeg.fadeOut(0);
		currBasket = $(document.elementFromPoint(pointer.pageX, pointer.pageY));
		currVeg.fadeIn(0);
		
		if (currBasket.attr("data-key") && successCondition(currVeg.attr("data-key"), currBasket.attr("data-key")))
				successFunction(currVeg, currBasket);
		else
			failFunction(currVeg, currBasket);
		
		currVeg.removeClass("box-shadow-white");
		currVeg.css("opacity", "");
		
		if (finishCondition())
		{
			finishFunction();
		}
	}
	
	this.onStart = function(instance, event, pointer)
	{
		this.vegetable = $(event.target);
		this.vegetable.css("z-index", "9999");
		this.vegetable.addClass("box-shadow-white");
		this.vegetable.css("opacity", "0.6");
	}
	
	this.makeThemDraggable();
	this.addEventListeners();
}

var blink = function(jqueryElements, interval, times)
{
	var intervalHalf = Math.round(interval/2);
	timeout[0] = setTimeout(function(){
		jqueryElements.css("outline", "3px solid red");
		timeout[1] = setTimeout(function(){
			jqueryElements.css("outline", "");
			if (times) 
				blink(jqueryElements, interval, --times)		
		}, intervalHalf);
	}, intervalHalf);
}

var setMenuStuff = function(){
	var enterButton = $(".enter-button"),
		annotation = $("#frame-000 .annotation-hidden"),
		annotationButton = $("#frame-000 .annotation-button"),
		password = $(".password"),
		error = $(".error"),
		name = $(".name");
		name.val("");
		password.val("");
		
		
		sayHello = function(){
		regBox.html("Здравствуйте, " + name.val() + "!");
		regBox.css("height", "3em");
		regBox.css("padding", "0.5em");
		regBox.css("text-align", "center");
		globalName = name.val();
		regBox.addClass("topcorner");
		timeout[0] = setTimeout(function(){
			regBox.html(name.val());
			regBox.css("width", name.val().length + "em");
		}, 3000);
	};
	
	if(globalName)
		sayHello();
	
	var annotationButtonListener = function(){
		annotation.toggleClass("annotation-shown");
		annotationButton.toggleClass("annotation-button-close");

	};
	annotationButton.off("click", annotationButtonListener);
	annotationButton.on("click", annotationButtonListener);
	
	var enterButtonListener = function(){
		if(password.val() === "12345")
			sayHello();
		else
			error.html("неверный пароль");
	};
	enterButton.off("click", enterButtonListener);
	enterButton.on("click", enterButtonListener);
}

var setLRSData = function(){
	tincan = new TinCan (
    {
        recordStores: [
            {
                "endpoint": "http://54.154.57.220/data/xAPI/",
				"username": "e083498f4e256e68ab2c5ae2be4195d9a348eb20",
				"password": "c6ea3c32a9d77919d0eb3cdea3bc5d460f50d93b"
            }
        ]
    });
}

var sendLaunchedStatement = function(sm)
{
	if(globalName)
	{
		tincan.sendStatement(
		{
			"actor": {
				"objectType": "Agent",
				"account": {
					"name": globalName,
					"homePage": "http://lcms.nis.kz"
				}
			},
			"verb": {
				"id": "http://adlnet.gov/expapi/verbs/launched"
			},
			"context": {
				"platform": "web"
			},
			"object": {
				"id": "http://lcms.nis.kz/activity/821eb006-58bb-4154-b732-4b2a9a9330ad",
				"objectType": "Activity",
				"description": "frame-" + sm
			}
		});
	}
}

var sendCompletedStatement = function(sm)
{
	if(globalName)
	{
		tincan.sendStatement(
		{
			"actor": {
				"objectType": "Agent",
				"account": {
					"name": globalName,
					"homePage": "http://lcms.nis.kz/chemistry"
				}
			},
			"verb": {
				"id": "http://adlnet.gov/expapi/verbs/completed"
			},
			"context": {
				"platform": "web"
			},
			"object": {
				"id": "http://lcms.nis.kz/activity/821eb006-58bb-4154-b732-4b2a9a9330ad",
				"objectType": "Activity",
				"description": "frame-" + sm
			}
		});
	}
}

function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays*24*60*60*1000));
    
	var expires = "expires="+d.toUTCString();
    
	document.cookie = cname + "=" + cvalue + "; " + expires;
}

function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for(var i=0; i<ca.length; i++) 
	{
        var c = ca[i];
        while (c.charAt(0)==' ') 
			c = c.substring(1);
        if (c.indexOf(name) == 0) 
			return c.substring(name.length, c.length);
    }
    return "";
}

launch["frame-000"] = function()
{
	
}

launch["frame-101"] = function()
{
		theFrame = $("#frame-101"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ",
		guideCloud = $(prefix + ".guide-cloud"),
		guideMouth = $(prefix + ".guide-mouth");
	
	typingText[0] = new TypingText(guideCloud, 100, false, function(){
		guideMouth.fadeOut(0);
		timeout[0] = setTimeout(function(){
			theFrame.attr("data-done", "true");
			hideEverythingBut($("#frame-102"));
			sendCompletedStatement(1);
		}, 3000);
	});
	
	guideCloud.fadeOut(0);
	fadeNavsOut();
	fadeLauncherIn();
	guideMouth.fadeOut(0);
		
	startButtonListener = function(){
		timeout[0] = setTimeout(function(){
			guideCloud.fadeIn(0);
			guideMouth.fadeIn(0);
			typingText[0].write();
		}, 2000);
	};
	timeout[0] = setTimeout(function(){
		startButtonListener();
		sendLaunchedStatement(1);
	}, 2000);
}

launch["frame-102"] = function()
{
		theFrame = $("#frame-102"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ",
		guide = $(prefix + ".s3-guide"),
		boyCloud = $(prefix + ".s3-boy-cloud"),
		guideCloud = $(prefix + ".s3-guide-cloud"),
		boyMouth = $(prefix + ".s3-boy-mouth"),
		guideMouth = $(prefix + ".s3-guide-mouth");
		capricorn = $(prefix + ".s2-golden-capricorn");
	
	typingText[0] = new TypingText(boyCloud, 100, false, function(){
		boyMouth.fadeOut(0);
		guide.fadeIn(0);
		timeout[0] = setTimeout(function(){
			capricorn.fadeIn(0);
			guideCloud.fadeIn(0);
			guideMouth.fadeIn(0);
			typingText[1].write();
		},1000);
	});
		
	typingText[1] = new TypingText(guideCloud, 100, false, function(){
		guideMouth.fadeOut(0);
		timeout[0] = setTimeout(function(){
			theFrame.attr("data-done", "true");
			hideEverythingBut($("#frame-103"));
			sendCompletedStatement(1);
		}, 5000);
	});
	
	guideCloud.fadeOut(0);
	boyCloud.fadeOut(0);
	guide.fadeOut(0);
	boyMouth.fadeOut(0);
	guideMouth.fadeOut(0);
	fadeNavsOut();
	fadeLauncherIn();
	
	startButtonListener = function(){
		timeout[0] = setTimeout(function(){
			boyCloud.fadeIn(0);
			boyMouth.fadeIn(0);
			typingText[0].write();
		}, 2000);
	};
	timeout[0] = setTimeout(function(){
		startButtonListener();
		sendLaunchedStatement(2);
	}, 2000);
}

launch["frame-103"] = function()
{
		theFrame = $("#frame-103"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ",
		boyCloud = $(prefix + ".boy-cloud"),
		guide = $(prefix + ".guide"),
		guideCloud = $(prefix + ".guide-cloud"),
		boyMouth = $(prefix + ".boy-mouth"),
		guideMouth = $(prefix + ".guide-mouth"),
		guideEyes = $(prefix + ".guide-eyes");
	
	typingText[0] = new TypingText(boyCloud, 100, false, function(){
		boyMouth.fadeOut(0);
		guide.fadeIn(0);
		timeout[0] = setTimeout(function(){
			guideCloud.fadeIn(0);
			guideMouth.fadeIn(0);
			guideEyes.fadeIn(0);
			typingText[1].write();
		}, 500);
	});
	
	typingText[1] = new TypingText(guideCloud, 100, false, function(){
			guideMouth.fadeOut(0);

		timeout[0] = setTimeout(function(){
			theFrame.attr("data-done", "true");
			fadeNavsInAuto();
			sendCompletedStatement(1);
		}, 5000);
	});
	
	boyCloud.fadeOut(0);
	guide.fadeOut(0);
	guideCloud.fadeOut(0);
	boyMouth.fadeOut(0);
	guideMouth.fadeOut(0);
	guideEyes.fadeOut(0);
	fadeNavsOut();
	fadeLauncherIn();
		
	startButtonListener = function(){
		timeout[0] = setTimeout(function(){
			boyCloud.fadeIn(0);
			typingText[0].write();
			boyMouth.fadeIn(0);
		}, 2000);
	};
	timeout[0] = setTimeout(function(){
		startButtonListener();
		sendLaunchedStatement(3);
	}, 2000);
}

launch["frame-104"] = function()
{
		theFrame = $("#frame-104"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ",
		guideCloud = $(prefix + ".guide-cloud"),
		guideMouth = $(prefix + ".guide-mouth"),
		guideEyes = $(prefix + ".guide-eyes"), 
		corrosionLabel = $(prefix + ".corrosion-label");
	
	typingText[0] = new TypingText(guideCloud, 100, false, function(){
		guideMouth.fadeOut(0);
		timeout[0] = setTimeout(function(){
			theFrame.attr("data-done", "true");
			fadeNavsInAuto();
			sendCompletedStatement(1);
		}, 5000);
	});
	
	fadeNavsOut();
	fadeLauncherIn();
	guideEyes.fadeOut(0);
	guideMouth.fadeOut(0);
	guideCloud.fadeOut(0);
	corrosionLabel.fadeOut(0);
		
	startButtonListener = function(){
		typingText[0].write();
		guideMouth.fadeIn(0);
		corrosionLabel.fadeIn(1000);
		guideCloud.fadeIn(0);

	};
	timeout[0] = setTimeout(function(){
		startButtonListener();
		sendLaunchedStatement(4);
	}, 2000);
}

launch["frame-105"] = function()
{
		theFrame = $("#frame-105"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ",
		taskCloud = $(prefix + "#s6-task-cloud"),
		items = $(prefix + ".item"),
		award = $(prefix + ".award"),
		mistakeNum = 0;
	
	fadeNavsOut();
	fadeLauncherIn();
	award.fadeOut(0);
		
	typingText[0] = new TypingText(taskCloud, 100, false, function(){
	});
	
	var itemListener = function(){
		currItem = $(this)
		if (currItem.attr("data-correct")) {
			currItem.css("border", "5px solid green");
			timeout[0] = setTimeout(function(){
				currItem.fadeOut(0);
				currItem.remove();
				var leftItems = $(prefix + ".metal");
				if(leftItems.length <= 0)
				{
					if (mistakeNum <= 1) {
						awardNum++;
						award.fadeIn(0);
						timeout[1] = setTimeout(function(){
							award.fadeOut(0);
							theFrame.attr("data-done", "true");
							fadeNavsInAuto();
							sendCompletedStatement(1);
						}, 3000);
					}
					else
					{
						theFrame.attr("data-done", "true");
						fadeNavsInAuto();
						sendCompletedStatement(1);
					}
				}
			}, 50)
		}
		else
		{
			currItem.css("border", "5px solid red");
			timeout[0] = setTimeout(function(){
				currItem.css("border", "");
			}, 500)
			mistakeNum++;
		}
	};
	
	items.off("click", itemListener);
	items.on("click", itemListener);
		
	startButtonListener = function(){
		timeout[0] = setTimeout(function(){
			typingText[0].write();
		}, 1000);
	};
	timeout[0] = setTimeout(function(){
		startButtonListener();
		sendLaunchedStatement(5);
	}, 2000);
}

launch["frame-106"] = function()
{
		theFrame = $("#frame-106"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ",
		girlCloud = $(prefix + ".girl-cloud"),
		girlMouth = $(prefix + ".girl-mouth"),
		items = $(prefix + ".item"),
		question = $(prefix + ".question");
	
	typingText[0] = new TypingText(girlCloud, 100, false, function(){

		girlMouth.fadeOut(0);
		question.fadeIn(500);
		timeout[0] = setTimeout(function(){
			theFrame.attr("data-done", "true");
			fadeNavsInAuto();
			sendCompletedStatement(1);
		}, 5000);
	});
	
	fadeNavsOut();
	fadeLauncherIn();
	girlMouth.fadeOut(0);
	items.fadeOut(0);
	question.fadeOut(0);
	girlCloud.fadeOut(0);
		
	startButtonListener = function(){
		timeout[0] = setTimeout(function(){
			girlCloud.fadeIn(0);
			typingText[0].write();
			girlMouth.fadeIn(0);
			timeout[1] = setTimeout(function(){
				items.fadeIn(500);
			}, 3000);
		}, 1000);
	};
	timeout[0] = setTimeout(function(){
		startButtonListener();
		sendLaunchedStatement(6);
	}, 2000);
}

launch["frame-107"] = function()
{
		theFrame = $("#frame-107"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ",
		teacherCloud = $(prefix + ".teacher-cloud"),
		teacherMouth = $(prefix + ".teacher-mouth"),
		teacherEyes = $(prefix + ".teacher-eyes"),
		series = $(prefix + ".series");
	
	teacherSprite = new Motio(teacherMouth[0], {
		"fps": "5",
		"frames": "2"
	});
	
	teacherEyesSprite = new Motio(teacherEyes[0], {
		"fps": "5",
		"frames": "2"
	});
	teacherEyesSprite.on("frame", function(){
		var currFrame = this;
		if (currFrame.frame === 0)
		{
			currFrame.pause();
			timeout[1] = setTimeout(function(){
				currFrame.play();
			}, 500);
		}
	});
	
	typingText[0] = new TypingText(teacherCloud, 100, false, function(){
		teacherMouth.fadeOut(0);
		teacherSprite.pause();
		timeout[2] = setTimeout(function(){
			theFrame.attr("data-done", "true");
			fadeNavsInAuto();
			sendCompletedStatement(1);
		}, 6000);
	});
	
	teacherMouth.fadeOut(0);
	teacherCloud.fadeOut(0);
	series.fadeOut(0);
	fadeNavsOut();
	fadeLauncherIn();
	
	teacherEyesSprite.play();
		
	startButtonListener = function(){
		typingText[0].write();
		teacherCloud.fadeIn(0);
		teacherMouth.fadeIn(0);
		teacherSprite.play();
		timeout[0] = setTimeout(function(){
			series.fadeIn(500);
		}, 5000);
	};
	timeout[0] = setTimeout(function(){
		startButtonListener();
		sendLaunchedStatement(7);
	}, 2000);
}

launch["frame-108"] = function()
{
		theFrame = $("#frame-108"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ",
		guideCloud1 = $(prefix + ".guide-cloud-1"),
		guideCloud2 = $(prefix + ".guide-cloud-2"),
		guideMouth = $(prefix + ".guide-mouth"),
		balls = $(prefix + ".ball"),
		baskets = $(prefix + ".basket");
	
	var successCondition = function(vegetable, basket)
	{
		return vegetable === basket;
	}
	
	var successFunction = function(vegetable, basket)
	{
		basket.css("background-image", vegetable.css("background-image"));
		basket.css("background-size", "100% 100%");
		vegetable.remove();
	}
	
	var failFunction = function(vegetable, basket)
	{
		vegetable.css("left", "");
		vegetable.css("top", "");
		//alert("Ошибочка!");

	}
	
	var finishCondition = function()
	{
		return !$(prefix + ".ball").length;
	}
	
	var finishFunction = function(vegetable, basket)
	{
		theFrame.attr("data-done", "true");
		fadeNavsInAuto();
		sendCompletedStatement(1);
	}
	
	var dragTask = new DragTask(balls, successCondition, successFunction, failFunction, finishCondition, finishFunction);
	
	typingText[0] = new TypingText(guideCloud1, 100, false, function(){
		guideMouth.fadeOut(0);
		timeout[0] = setTimeout(function(){
			guideCloud1.fadeOut(0);
			guideCloud2.fadeIn(0);
			typingText[1].write();
			guideMouth.fadeIn(0);
			blink(balls, 500, 4);
		}, 2000);
		timeout[1] = setTimeout(function(){
			blink(baskets, 500, 4);
		}, 5000);
	});
	
	typingText[1] = new TypingText(guideCloud2, 100, false, function(){
		guideMouth.fadeOut(0);
	});
	
	fadeNavsOut();
	fadeLauncherIn();
	guideMouth.fadeOut(0);
	guideCloud1.fadeOut(0);
	guideCloud2.fadeOut(0);
		
	startButtonListener = function(){
		timeout[0] = setTimeout(function(){
			typingText[0].write();
			guideMouth.fadeIn(0);
			guideCloud1.fadeIn(0);
		}, 1000);
	};
	timeout[0] = setTimeout(function(){
		startButtonListener();
		sendLaunchedStatement(8);
	}, 2000);
}

launch["frame-109"] = function()
{
		theFrame = $("#frame-109"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ",
		teacherCloud2 = $(prefix + ".teacher-cloud-2"),
		teacherMouth = $(prefix + ".teacher-mouth"),
		beketovPortrait = $(prefix + ".beketov-portait"),
		beketovNameYears = $(prefix + ".beketov-name-years"),
		blueHighlight = $(prefix + ".series-blue-highlight"),
		greenHighlight = $(prefix + ".series-green-highlight"),
		push = $(prefix + ".push"),
		nopush = $(prefix + ".no-push");
	
	typingText[0] = new TypingText(teacherCloud2, 100, false, function(){
		teacherMouth.fadeOut(0);
		timeout[0] = setTimeout(function(){
			theFrame.attr("data-done", "true");
			fadeNavsInAuto();
			sendCompletedStatement(1);
		}, 7000);
	});
	
	fadeNavsOut();
	fadeLauncherIn();
	teacherCloud2.fadeOut(0);
	blueHighlight.fadeOut(0);
	greenHighlight.fadeOut(0);
	push.fadeOut(0);
	nopush.fadeOut(0);
	teacherMouth.fadeOut(0);
	beketovPortrait.fadeOut(0);
	beketovNameYears.fadeOut(0);
		
	startButtonListener = function(){
		teacherCloud2.fadeIn(0);
		typingText[0].write();
		teacherMouth.fadeIn(0);
		timeout[2] = setTimeout(function(){
			beketovPortrait.fadeIn(500);	
		}, 3000);
		timeout[3] = setTimeout(function(){
			beketovNameYears.fadeIn(500);	
		}, 4000);
		timeout[4] = setTimeout(function(){
			greenHighlight.fadeIn(500);	
			push.fadeIn(500);
		}, 25600);
		timeout[5] = setTimeout(function(){
			blueHighlight.fadeIn(500);	
			nopush.fadeIn(500);
		}, 31800);
	};
	timeout[0] = setTimeout(function(){
		startButtonListener();
		sendLaunchedStatement(9);
	}, 2000);
}

launch["frame-110"] = function()
{
		theFrame = $("#frame-110"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ",
		boyCloud = $(prefix + ".boy-cloud"),
		boyMouth = $(prefix + ".boy-mouth"),
		ring = $(prefix + ".ring"),
		spoon = $(prefix + ".spoon"), 
		door = $(prefix + ".door"),
		caserole = $(prefix + ".caserole"),
		cable = $(prefix + ".cable"),
		natrium = $(prefix + ".natrium"),
		kalium = $(prefix + ".kalium"),
		calcium = $(prefix + ".calcium"),
		items = $(prefix + ".item");
	
	typingText[0] = new TypingText(boyCloud, 100, false, function(){
		boyMouth.fadeOut(0);
		timeout[8] = setTimeout(function(){
			theFrame.attr("data-done", "true");
			fadeNavsInAuto();
			sendCompletedStatement(1);
		}, 5000);
	});
	
	boyCloud.fadeOut(0);
	boyMouth.fadeOut(0);
	items.fadeOut(0);
	fadeNavsOut();
	fadeLauncherIn();
		
	startButtonListener = function(){
		timeout[0] = setTimeout(function(){
			boyCloud.fadeIn(0);
			typingText[0].write();
			boyMouth.fadeIn(0);
		}, 1000);
		timeout[0] = setTimeout(function(){
			ring.fadeIn(500);
		}, 6000);
		timeout[1] = setTimeout(function(){
			spoon.fadeIn(500);
		}, 7000);
		timeout[2] = setTimeout(function(){
			door.fadeIn(500);
		}, 8000);
		timeout[3] = setTimeout(function(){
			caserole.fadeIn(500);
		}, 9000);
		timeout[4] = setTimeout(function(){
			cable.fadeIn(500);
		}, 10000);
		timeout[5] = setTimeout(function(){
			natrium.fadeIn(500);
		}, 14000);
		timeout[6] = setTimeout(function(){
			kalium.fadeIn(500);
		}, 15000);
		timeout[7] = setTimeout(function(){
			calcium.fadeIn(500);
		}, 16000);
	};
	timeout[0] = setTimeout(function(){
		startButtonListener();
		sendLaunchedStatement(10);
	}, 2000);
}

launch["frame-111"] = function()
{
		theFrame = $("#frame-111"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ",
		guideMouth = $(prefix + ".guide-mouth"),
		activeHighlight = $(prefix + ".active-metals-highlight"),
		midActiveHighlight = $(prefix + ".middle-metals-highlight"),
		inactiveHighlight = $(prefix + ".inactive-metals-highlight"),
		guideCloud1 = $(prefix + ".guide-cloud-1"),
		guideCloud2 = $(prefix + ".guide-cloud-2"),
		guideCloud3 = $(prefix + ".guide-cloud-3"),
		active = $(prefix + ".active"),
		middleActive = $(prefix + ".middle-active"),
		inactive = $(prefix + ".inactive");
	
	typingText[0] = new TypingText(guideCloud1, 100, false, function(){
		guideMouth.fadeOut(0);
		timeout[1] = setTimeout(function(){
			guideMouth.fadeIn(0);
			guideCloud2.fadeIn(0);
			typingText[1].write();
			timeout[2] = setTimeout(function(){
				midActiveHighlight.fadeIn(500);
				middleActive.fadeIn(500);
			}, 2000);
		}, 1000);
	});
	typingText[1] = new TypingText(guideCloud2, 100, false, function(){
		guideMouth.fadeOut(0);
		timeout[3] = setTimeout(function(){
			guideMouth.fadeIn(0);
			guideCloud3.fadeIn(0);
			typingText[2].write();
			timeout[4] = setTimeout(function(){
				inactiveHighlight.fadeIn(500);
				inactive.fadeIn(500);
			}, 2000);
		}, 1000);
	});
	typingText[2] = new TypingText(guideCloud3, 100, false, function(){
		guideMouth.fadeOut(0);
		timeout[5] = setTimeout(function(){
			theFrame.attr("data-done", "true");
			fadeNavsInAuto();
			sendCompletedStatement(1);
		}, 6000);
	});
	
	fadeNavsOut();
	fadeLauncherIn();
	guideMouth.fadeOut(0);
	activeHighlight.fadeOut(0);
	midActiveHighlight.fadeOut(0);
	inactiveHighlight.fadeOut(0);
	guideCloud1.fadeOut(0);
	guideCloud2.fadeOut(0);
	guideCloud3.fadeOut(0);
	active.fadeOut(0);
	middleActive.fadeOut(0);
	inactive.fadeOut(0);
		
	startButtonListener = function(){
		timeout[0] = setTimeout(function(){
			guideCloud1.fadeIn(0);
			typingText[0].write();
			guideMouth.fadeIn(0);
		}, 1000);
				
		timeout[0] = setTimeout(function(){
			active.fadeIn(500);
			activeHighlight.fadeIn(500);
		}, 8000);
	};
	timeout[0] = setTimeout(function(){
		startButtonListener();
		sendLaunchedStatement(11);
	}, 2000);
}

launch["frame-112"] = function()
{
		theFrame = $("#frame-112"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ",
		teacherCloud = $(prefix + ".teacher-cloud"),
		teacherMouth = $(prefix + ".teacher-mouth"),
		videoContainer = $(prefix + ".video-container"),
		experiment = $(prefix + ".experiment");
	
	experiment.attr("width", videoContainer.css("width"));
	experiment.attr("height", videoContainer.css("height"));
	
	$(window).resize(function(){
		experiment.attr("width", videoContainer.css("width"));
		experiment.attr("height", videoContainer.css("height"));
	});
	
	typingText[0] = new TypingText(teacherCloud, 100, false, function(){
		teacherMouth.fadeOut(0);
		timeout[0] = setTimeout(function(){
			videoContainer.css("width", "100%");
			videoContainer.css("height", "100%");
			videoContainer.css("left", "0%");
			videoContainer.css("top", "0%");
			experiment.attr("width", videoContainer.css("width"));
			experiment.attr("height", videoContainer.css("height"));
			experiment[0].play();
		}, 3000);
	});
	
	fadeNavsOut();
	fadeLauncherIn();
	teacherMouth.fadeOut(0);
	
	experiment[0].addEventListener("ended", function(){
		theFrame.attr("data-done", "true");
		fadeNavsInAuto();
		sendCompletedStatement(1);
	});
		
	startButtonListener = function(){
		timeout[0] = setTimeout(function(){
			typingText[0].write();
			teacherMouth.fadeIn(0);
		}, 1000);
	};
	timeout[0] = setTimeout(function(){
		startButtonListener();
		sendLaunchedStatement(12);
	}, 2000);
}

launch["frame-113"] = function()
{
		theFrame = $("#frame-113"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ",
		boyCloud2 = $(prefix + ".boy-cloud-2"),
		boyMouth = $(prefix + ".boy-mouth"),
		directions = $(prefix + ".directions"),
		boxes = $(prefix + ".box"),
		award = $(prefix + ".award"),
		mistakesNum = 0,
		correctNum = 0;
	
	typingText[0] = new TypingText(boyCloud2, 100, false, function(){
		boyMouth.fadeOut(0);
		boxes.fadeIn(1000);
		directions.fadeIn(0);
	});
	
	var boxListener = function(){
		currBox = $(this);
		if (currBox.attr("data-correct"))
		{
			currBox.css("background-color", "green");
			currBox.css("color", "white");
			currBox.off("click", boxListener);
			correctNum ++;
		}
		else
		{
			currBox.css("background-color", "red");
			currBox.css("color", "white");
			mistakesNum ++;
			timeout[1] = setTimeout(function(){
				currBox.css("background-color", "");
				currBox.css("color", "");
			}, 1000);
		}
		if (correctNum >= 5)
		{
			if (mistakesNum <= 1) {
				awardNum++;
				award.fadeIn(500);
				timeout[2] = setTimeout(function(){
					award.fadeOut(0);
					theFrame.attr("data-done", "true");
					fadeNavsInAuto();
					sendCompletedStatement(1);
				}, 4000);
			}
			else
			{
				theFrame.attr("data-done", "true");
				fadeNavsInAuto();
				sendCompletedStatement(1);
			}
		}
	}
	
	boxes.off("click", boxListener);
	boxes.on("click", boxListener);
	
	fadeNavsOut();
	fadeLauncherIn();
	boyCloud2.fadeOut(0);
	boyMouth.fadeOut(0);
	directions.fadeOut(0);
	boxes.fadeOut(0);
	award.fadeOut(0);
		
	startButtonListener = function(){
		boyCloud2.fadeIn(0);
		typingText[0].write();
		boyMouth.fadeIn(0);
	};
	timeout[0] = setTimeout(function(){
		startButtonListener();
		sendLaunchedStatement(13);
	}, 2000);
}

launch["frame-114"] = function()
{
		theFrame = $("#frame-114"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ",
		guideCloud = $(prefix + ".guide-cloud"),
		girlCloud = $(prefix + ".girl-cloud"),
		boyCloud = $(prefix + ".boy-cloud"),
		guideMouth = $(prefix + ".guide-mouth"),
		girlMouth = $(prefix + ".girl-mouth"),
		boyMouth = $(prefix + ".boy-mouth");
	
	typingText[0] = new TypingText(guideCloud, 100, false, function(){
		guideMouth.fadeOut(0);
		timeout[0] = setTimeout(function(){
			girlMouth.fadeIn(0);
			girlCloud.fadeIn(0);
			typingText[2].write();
		}, 1000);
	});
	typingText[1] = new TypingText(boyCloud, 100, false, function(){
		boyMouth.fadeOut(0);
		timeout[0] = setTimeout(function(){
			guideMouth.fadeIn(0);
			guideCloud.fadeIn(0);
			typingText[0].write();
		}, 1000);
	});
	typingText[2] = new TypingText(girlCloud, 100, false, function(){
		girlMouth.fadeOut(0);
		timeout[0] = setTimeout(function(){
			theFrame.attr("data-done", "true");
			fadeNavsInAuto();
			sendCompletedStatement(1);
		}, 5000);
	});
	
	fadeNavsOut();
	fadeLauncherIn();
	boyCloud.fadeOut(0);
	girlCloud.fadeOut(0);
	guideCloud.fadeOut(0);
	girlMouth.fadeOut(0);
	guideMouth.fadeOut(0);
	boyMouth.fadeOut(0);
		
	startButtonListener = function(){
		typingText[1].write();
		boyCloud.fadeIn(0);
		boyMouth.fadeIn(0);
	};
	timeout[0] = setTimeout(function(){
		startButtonListener();
		sendLaunchedStatement(14);
	}, 2000);
}

launch["frame-115"] = function()
{
		theFrame = $("#frame-115"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ",
		guideCloud = $(prefix + ".guide-cloud"),
		guideMouth = $(prefix + ".guide-mouth"),
		boxes = $(prefix + ".box"),
		part1 = $(prefix + ".part-1"),
		part2 = $(prefix + ".part-2"),
		example = $(prefix + ".s15-for-example");
		
	typingText[0] = new TypingText(guideCloud, 100, false, function(){
		guideMouth.fadeOut(0);
		fadeOneByOne(part1, 0, 500, function(){});

		timeout[0] = setTimeout(function () {
			example.fadeIn(0);
		}, 6000);

		timeout[0] = setTimeout(function(){
			fadeOneByOne(part2, 0, 500, function(){});
		}, 6000);

		timeout[0] = setTimeout(function(){
			theFrame.attr("data-done", "true");
			fadeNavsInAuto();
			sendCompletedStatement(1);
		}, 15000);
	});
	
	fadeNavsOut();
	fadeLauncherIn();
	guideCloud.fadeOut(0);
	guideMouth.fadeOut(0);
	part1.fadeOut(0);
	part2.fadeOut(0);
	example.fadeOut(0);
		
	startButtonListener = function(){
		guideCloud.fadeIn(0);
		typingText[0].write();
		guideMouth.fadeIn(0);
	};
	timeout[0] = setTimeout(function(){
		startButtonListener();
		sendLaunchedStatement(15);
	}, 2000);
}

launch["frame-116"] = function()
{
		theFrame = $("#frame-116"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ",
		guideCloud1 = $(prefix + ".girl-cloud-1"),
		guideCloud2 = $(prefix + ".girl-cloud-2"),
		guideMouth = $(prefix + ".guide-mouth"),
		girlMouth = $(prefix + ".girl-mouth"),
		questions = $(prefix + ".questions"),
		boxes = $(prefix + ".box"),
		balls = $(prefix + ".ball"),
		mistakesNum = 0,
		award = $(prefix + ".award");
	
	var successCondition = function(vegetable, basket){
		return vegetable === basket;
	};
	
	var successFunction = function(vegetable, basket){
		basket.html(vegetable.html());
		vegetable.css("left", "");
		vegetable.css("top", "");
		vegetable.css("z-index", "");
	};
	
	var failFunction = function(vegetable, basket){
		// mistakesNum ++;
		// alert("Ошибок: "+mistakesNum);
		vegetable.css("left", "");
		vegetable.css("top", "");
		vegetable.css("z-index", "");
	};
	
	var finishCondition = function(){
		return allHaveHtml($(prefix + ".basket")); 
	};
	
	var finishFunction = function(){
		if (mistakesNum <= 2)
		{
			awardNum ++;
			award.fadeIn(500);
			timeout[0] = setTimeout(function(){
				award.fadeOut(0);
				theFrame.attr("data-done", "true");
				fadeNavsInAuto();
				sendCompletedStatement(1);
			}, 3000);
		}
		else
		{
			theFrame.attr("data-done", "true");
			fadeNavsInAuto();
			sendCompletedStatement(1);
		}
	};
	
	var dragTask = new DragTask(balls, successCondition, successFunction, failFunction, finishCondition,finishFunction);
	
	typingText[0] = new TypingText(guideCloud1, 100, false, function(){
		girlMouth.fadeOut(0);
		timeout[0] = setTimeout(function(){
			typingText[1].write();
			girlMouth.fadeIn(0);
			guideCloud2.fadeIn(0);
		}, 2000);
	});
	
	typingText[1] = new TypingText(guideCloud2, 100, false, function(){
		girlMouth.fadeOut(0);
		questions.fadeIn(1000);
		boxes.fadeIn(1000);
	});
	
	fadeNavsOut();
	fadeLauncherIn();
	guideCloud1.fadeOut(0);
	guideCloud2.fadeOut(0);
	girlMouth.fadeOut(0);
	award.fadeOut(0);
	questions.fadeOut(0);
	boxes.fadeOut(0);
		
	startButtonListener = function(){
		timeout[0] = setTimeout(function(){
			typingText[0].write();
			guideCloud1.fadeIn(0);
			girlMouth.fadeIn(0);
		}, 1000);
	};
	timeout[0] = setTimeout(function(){
		startButtonListener();
		sendLaunchedStatement(16);
	}, 2000);
}

launch["frame-117"] = function()
{
		theFrame = $("#frame-117"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ",
		teacherCloud = $(prefix + ".teacher-cloud"),
		boyCloud = $(prefix + ".boy-cloud"),
		boyMouth = $(prefix + ".boy-mouth"),
		teacherMouth = $(prefix + ".teacher-mouth"),
		
		boy = $(prefix + ".boy"),
		boyEyes = $(prefix + ".boy-eyes"),

		girl = $(prefix + ".s17-main-girl"),
		girlCloud = $(prefix + ".s17-girl-cloud"),
		girlMouth = $(prefix + ".s17-girl-mouth"),
		girlEyes = $(prefix + ".s17-girl-eyes");
		
	typingText[0] = new TypingText(teacherCloud, 100, false, function(){
		teacherMouth.fadeOut(0);
		timeout[0] = setTimeout(function(){
			boy.fadeIn(0);
			boyCloud.fadeIn(0);
			typingText[1].write();
			boyMouth.fadeIn(0);
			boyEyes.fadeIn(0);
		}, 1000);
	});
	
	typingText[1] = new TypingText(boyCloud, 100, false, function(){
		boyMouth.fadeOut(0);
		timeout[0] = setTimeout(function(){
			girl.fadeIn(0);
			girlCloud.fadeIn(0);
			girlMouth.fadeIn(0);
			girlEyes.fadeIn(0);
			typingText[2].write();
		}, 1000);
	});
	
	typingText[2] = new TypingText(girlCloud, 100, false, function(){
		girlMouth.fadeOut(0);
		timeout[0] = setTimeout(function(){
			theFrame.attr("data-done", "true");
			fadeNavsInAuto();
			sendCompletedStatement(1);
		}, 5000);
	});

	fadeNavsOut();
	fadeLauncherIn();

	teacherCloud.fadeOut(0);
	teacherMouth.fadeOut(0);

	boy.fadeOut(0);
	boyEyes.fadeOut(0);
	boyMouth.fadeOut(0);
	boyCloud.fadeOut(0);
	
	girl.fadeOut(0);
	girlMouth.fadeOut(0);
	girlEyes.fadeOut(0);
	girlCloud.fadeOut(0);

		
	startButtonListener = function(){
		timeout[0] = setTimeout(function(){
			teacherMouth.fadeIn(0);
			teacherCloud.fadeIn(0);
			typingText[0].write();
		}, 1000);
	};
	timeout[0] = setTimeout(function(){
		startButtonListener();
		sendLaunchedStatement(17);
	}, 2000);
}

launch["frame-118"] = function() 
{
		theFrame = $("#frame-118"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ",
		guideCloud = $(prefix + ".guide-cloud"),
		guideMouth = $(prefix + ".guide-mouth"),
		guideEyes = $(prefix + ".guide-eyes"),
		guide = $(prefix + ".guide"),
		videoContainer = $(prefix + ".video-container"),
		experiment = $(prefix + ".video-container .video");

		experiment.attr("width", videoContainer.css("width"));
		experiment.attr("height", videoContainer.css("height"));
	
	$(window).resize(function(){
		experiment.attr("width", videoContainer.css("width"));
		experiment.attr("height", videoContainer.css("height"));
	});

	experiment[0].addEventListener("ended",function(){
		timeout[1] = setTimeout(function(){
			theFrame.attr("data-done", "true");
			fadeNavsInAuto();
			sendCompletedStatement(1);
		}, 2000);	
	});

	typingText[0] = new TypingText(guideCloud, 100, false, function(){
		guideMouth.fadeOut(0);
		timeout[0] = setTimeout(function(){
			videoContainer.fadeIn(0);
			experiment[0].play();
		}, 1500);
	});
	
	
	guideCloud.fadeOut(0);
	guideMouth.fadeOut(0);
	guide.fadeOut(0);
	guideEyes.fadeOut(0);
	videoContainer.fadeOut(0);
	fadeNavsOut();
	fadeLauncherIn();
	
	startButtonListener = function(){
		timeout[0] = setTimeout(function(){
			guideMouth.fadeIn(0);
			guideCloud.fadeIn(0);
			typingText[0].write();	
		}, 1000);	
	};
	timeout[0] = setTimeout(function(){
		startButtonListener();
		sendLaunchedStatement(18);
	}, 2000);
}

launch["frame-119"] = function()
{
		theFrame = $("#frame-119"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ",
		teacherCloud = $(prefix + ".teacher-cloud"),
		teacherMouth = $(prefix + ".teacher-mouth"),
		teacherEyes = $(prefix + ".teacher-eyes"),
		part1 = $(prefix + ".part-1"),
		part2 = $(prefix + ".part-2");
	
	typingText[0] = new TypingText(teacherCloud, 100, false, function(){
		teacherMouth.fadeOut(0);
		timeout[0] = setTimeout(function(){
			fadeOneByOne(part1, 0, 500, function(){
				timeout[1] = setTimeout(function(){
					fadeOneByOne(part2, 0, 500, function(){
						timeout[2] = setTimeout(function(){
							theFrame.attr("data-done", "true");
							fadeNavsInAuto();
							sendCompletedStatement(1);
						}, 5000);
					})
				}, 2000);
			});
		}, 2000);
	});
	
	fadeNavsOut();
	fadeLauncherIn();
	teacherEyes.fadeOut(0);
	teacherMouth.fadeOut(0);
	teacherCloud.fadeOut(0);
	part1.fadeOut(0);
	part2.fadeOut(0);
		
	startButtonListener = function(){
		teacherCloud.fadeIn(0);
		teacherMouth.fadeIn(0);
		teacherMouth.fadeIn(0);

		typingText[0].write();
	};
	timeout[0] = setTimeout(function(){
		startButtonListener();
		sendLaunchedStatement(19);
	}, 2000);
}

launch["frame-120"] = function()
{
		theFrame = $("#frame-120"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ",
		boyCloud2 = $(prefix + ".boy-cloud-2"),
		boyMouth = $(prefix + ".boy-mouth"),
		balls = $(prefix + ".ball"),
		boxes = $(prefix + ".box"),
		question = $(prefix + ".question"),
		mistakesNum = 0,
		award = $(prefix + ".award");
	
	var successCondition = function(vegetable, basket){
		return vegetable === basket;
	};
	
	var successFunction = function(vegetable, basket){
		basket.html(vegetable.html());
		vegetable.css("left", "");
		vegetable.css("top", "");
		vegetable.css("z-index", "");
	};
	
	var failFunction = function(vegetable, basket){
		// mistakesNum ++;
		// alert("Ошибок: "+ mistakesNum);
		vegetable.css("left", "");
		vegetable.css("top", "");
		vegetable.css("z-index", "");
	};
	
	var finishCondition = function(){
		return allHaveHtml($(prefix + ".basket")); 
	};
	
	var finishFunction = function(){
		if (mistakesNum <= 2)
		{
			awardNum ++;
			award.fadeIn(500);
			timeout[0] = setTimeout(function(){
				award.fadeOut(0);
				theFrame.attr("data-done", "true");
				fadeNavsInAuto();
				sendCompletedStatement(1);
			}, 3000);
		}
		else
		{
			theFrame.attr("data-done", "true");
			fadeNavsInAuto();
			sendCompletedStatement(1);
		}
	};
	
	var dragTask = new DragTask(balls, successCondition, successFunction, failFunction, finishCondition,finishFunction);
	
	typingText[0] = new TypingText(boyCloud2, 100, false, function(){
		boyMouth.fadeOut(0);
		boxes.fadeIn(1000);
		question.fadeIn(1000);
	});
	
	fadeNavsOut();
	fadeLauncherIn();
	boyMouth.fadeOut(0);
	boyCloud2.fadeOut(0);
	award.fadeOut(0);
	boxes.fadeOut(0);
	question.fadeOut(0);
		
	startButtonListener = function(){
		boyCloud2.fadeIn(0);
		typingText[0].write();
		boyMouth.fadeIn(0);
	};
	timeout[0] = setTimeout(function(){
		startButtonListener();
		sendLaunchedStatement(20);
	}, 2000);
}

launch["frame-120-b"] = function()
{
		theFrame = $("#frame-120-b"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ",
		balls = $(prefix + ".ball"),
		boxes = $(prefix + ".box"),
		question = $(prefix + ".question"),
		mistakesNum = 0,
		award = $(prefix + ".award");
	
	var successCondition = function(vegetable, basket){
		return vegetable === basket;
	};
	
	var successFunction = function(vegetable, basket){
		basket.html(vegetable.html());
		vegetable.css("left", "");
		vegetable.css("top", "");
		vegetable.css("z-index", "");
	};
	
	var failFunction = function(vegetable, basket){
		// mistakesNum ++;
		// alert("Ошибок: "+ mistakesNum);
		vegetable.css("left", "");
		vegetable.css("top", "");
		vegetable.css("z-index", "");
	};
	
	var finishCondition = function(){
		return allHaveHtml($(prefix + ".basket")); 
	};
	
	var finishFunction = function(){
		if (mistakesNum <= 2)
		{
			awardNum++;
			award.fadeIn(500);
			timeout[0] = setTimeout(function(){
				award.fadeOut(0);
			}, 3000);
		}
		theFrame.attr("data-done", "true");
		fadeNavsInAuto();
		sendCompletedStatement(1);
	};
	
	var dragTask = new DragTask(balls, successCondition, successFunction, failFunction, finishCondition,finishFunction);
	
	fadeNavsOut();
	fadeLauncherIn();
	award.fadeOut(0);
	boxes.fadeOut(0);
	question.fadeOut(0);
		
	startButtonListener = function(){
		boxes.fadeIn(1000);
		question.fadeIn(1000);
	};
	timeout[0] = setTimeout(function(){
		startButtonListener();
		sendLaunchedStatement(200);
	}, 2000);
}

launch["frame-121"] = function()
{
		theFrame = $("#frame-121"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ",
		girlCloud = $(prefix + ".girl-cloud"),
		girlMouth = $(prefix + ".girl-mouth");
	
	typingText[0] = new TypingText(girlCloud, 100, false, function(){
		girlMouth.fadeOut(0);
		timeout[0] = setTimeout(function(){
			theFrame.attr("data-done", "true");
			fadeNavsInAuto();
			sendCompletedStatement(1);
		}, 5000);
	});
	
	fadeNavsOut();
	fadeLauncherIn();
	girlCloud.fadeOut(0);
	girlMouth.fadeOut(0);
		
	startButtonListener = function(){
		timeout[0] = setTimeout(function(){
			girlMouth.fadeIn(0);
			girlCloud.fadeIn(0);
			typingText[0].write();
		}, 1000);
	};
	timeout[0] = setTimeout(function(){
		startButtonListener();
		sendLaunchedStatement(21);
	}, 2000);
}

launch["frame-122"] = function()
{
		theFrame = $("#frame-122"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ",
		guideCloud = $(prefix + ".guide-cloud"),
		guideMouth = $(prefix + ".guide-mouth"),
		videoContainer = $(prefix + ".video-container"),
		experiment = $(prefix + ".experiment");
	
	experiment.attr("width", videoContainer.css("width"));
	experiment.attr("height", videoContainer.css("height"));
	
	$(window).resize(function(){
		experiment.attr("width", videoContainer.css("width"));
		experiment.attr("height", videoContainer.css("height"));
	});
	
	typingText[0] = new TypingText(guideCloud, 100, false, function(){
		guideMouth.fadeOut(0);
		timeout[0] = setTimeout(function(){
			videoContainer.css("width", "100%");
			videoContainer.css("height", "100%");
			videoContainer.css("left", "0%");
			videoContainer.css("top", "0%");
			experiment.attr("width", videoContainer.css("width"));
			experiment.attr("height", videoContainer.css("height"));
			experiment[0].play();
		}, 3000);
	});
	
	experiment[0].addEventListener("ended", function(){
		theFrame.attr("data-done", "true");
		fadeNavsInAuto();
		sendCompletedStatement(1);
	});
	
	fadeNavsOut();
	fadeLauncherIn();
	guideCloud.fadeOut(0);
	guideMouth.fadeOut(0);
		
	startButtonListener = function(){
		timeout[0] = setTimeout(function(){
			typingText[0].write();
			guideCloud.fadeIn(0);
			guideMouth.fadeIn(0);
		}, 1000);
	};
	timeout[0] = setTimeout(function(){
		startButtonListener();
		sendLaunchedStatement(22);
	}, 2000);
}

launch["frame-123"] = function()
{
		theFrame = $("#frame-123"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ",
		guideCloud = $(prefix + ".guide-cloud"),
		guideEyes = $(prefix + ".guide-eyes"),
		guide = $(prefix + ".guide"),
		guideMouth = $(prefix + ".guide-mouth"),
		girlCloud = $(prefix + ".girl-cloud"),
		girlMouth = $(prefix + ".girl-mouth");
	
	typingText[0] = new TypingText(guideCloud, 100, false, function(){
		guideMouth.fadeOut(0);
		timeout[0] = setTimeout(function(){
			theFrame.attr("data-done", "true");
			fadeNavsInAuto();
			sendCompletedStatement(1);
		}, 5000);
	});
	
	typingText[1] = new TypingText(girlCloud, 100, false, function(){
		girlMouth.fadeOut(0);
		timeout[0] = setTimeout(function(){
			guide.fadeIn(0);
			guideCloud.fadeIn(0);
			guideEyes.fadeIn(0);
			typingText[0].write();
			guideMouth.fadeIn(0);
		}, 2000);
	});
	
	fadeNavsOut();
	fadeLauncherIn();
	girlMouth.fadeOut(0);
	girlCloud.fadeOut(0);
	guideMouth.fadeOut(0);
	guideEyes.fadeOut(0);
	guide.fadeOut(0);
	guideCloud.fadeOut(0);
		
	startButtonListener = function(){
		timeout[0] = setTimeout(function(){
			girlCloud.fadeIn(0);
			girlMouth.fadeIn(0);
			typingText[1].write();
		}, 1000);
	};
	timeout[0] = setTimeout(function(){
		startButtonListener();
		sendLaunchedStatement(23);
	}, 2000);
}

launch["frame-124"] = function()
{
		theFrame = $("#frame-124"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ",
		teacherCloud = $(prefix + ".teacher-cloud"),
		teacherMouth = $(prefix + ".teacher-mouth"),
		textfields = $(prefix + ".textfield"),
		nextStep = $(prefix + ".next-step-button"),
		prevStep = $(prefix + ".prev-step-button"),
		saveStep = $(prefix + ".save-step-button"),
		steps = $(prefix + ".step"),
		printButton = $(prefix + ".print-button"),
		activeStepNum = 1,
		activeStep = $(prefix + ".step-" + activeStepNum);
	
	textfields.val("");
	
	typingText[0] = new TypingText(teacherCloud, 100, false, function(){
		teacherMouth.fadeOut(0);
		timeout[0] = setTimeout(function(){
			activeStep.fadeIn(500);
		}, 1000);
	});
	
	var nextStepListener = function(){
		if (activeStepNum <= 3) 
			activeStepNum ++;
		
		activeStep = $(prefix + ".step-" + activeStepNum);
		steps.fadeOut(0);
		activeStep.fadeIn(500);
	}
	nextStep.off("click", nextStepListener);
	nextStep.on("click", nextStepListener);
	
	var prevStepListener = function(){
		if (activeStepNum >= 0) 
			activeStepNum --;
		activeStep = $(prefix + ".step-" + activeStepNum);
		steps.fadeOut(0);
		activeStep.fadeIn(500);
	}
	prevStep.off("click", prevStepListener);
	prevStep.on("click", prevStepListener);	
	
	var saveStepListener = function(){
		if (activeStepNum >= 0) 
			activeStepNum ++;
		activeStep = $(prefix + ".step-" + activeStepNum);
		steps.fadeOut(0);
		activeStep.fadeIn(500);
		
		answertext1 = $(textfields[0]).val();
		answertext2 = $(textfields[1]).val();
		answertext3 = $(textfields[2]).val();
		
		setCookie("answer1", answertext1, 2);
		setCookie("answer2", answertext2, 2);
		setCookie("answer3", answertext3, 2);
		
		printButton.fadeIn(500);

		timeout[0] = setTimeout(function(){
			theFrame.attr("data-done", "true");
			fadeNavsInAuto();
			sendCompletedStatement(1);
		}, 15000);	
	}
	
	saveStep.off("click", saveStepListener);
	saveStep.on("click", saveStepListener);	
	
	fadeNavsOut();
	fadeLauncherIn();
	teacherCloud.fadeOut(0);
	teacherMouth.fadeOut(0);
	steps.fadeOut(0);
	printButton.fadeOut(0);
	
	var printButtonListener = function()
	{
		var win = window.open("answersToPrint.html", '_blank');
		win.focus();
	}
	
	printButton.off("click", printButtonListener);
	printButton.on("click", printButtonListener);
	
	startButtonListener = function(){
		timeout[0] = setTimeout(function(){
			teacherMouth.fadeIn(0);
			teacherCloud.fadeIn(0);
			typingText[0].write();
		}, 1000);
	};
	timeout[0] = setTimeout(function(){
		startButtonListener();
		sendLaunchedStatement(24);
	}, 2000);
}

launch["frame-125"] = function()
{
		theFrame = $("#frame-125"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ",
		teacherCloud = $(prefix + ".teacher-cloud"),
		cloud1 = $(prefix + ".cloud-1"),
		cloud2 = $(prefix + ".cloud-2"),
		cloud3 = $(prefix + ".cloud-3"),
		teacherMouth = $(prefix + ".teacher-mouth"),
		boxes = $(prefix + ".box"),
		result = $(prefix + ".result");
	
	if(awardNum === 0)
	{
		typingText[0] = new TypingText(cloud1, 100, false, function(){
			teacherMouth.fadeOut(0);
		});
	}
	else if(awardNum < 4)
	{
		typingText[0] = new TypingText(cloud2, 100, false, function(){
			teacherMouth.fadeOut(0);
			timeout[0] = setTimeout(function(){
				boxes.fadeIn(1000);
			}, 1000);
		});
	}
	else
	{
		typingText[0] = new TypingText(cloud3, 100, false, function(){
			teacherMouth.fadeOut(0);
			timeout[0] = setTimeout(function(){
				boxes.fadeIn(1000);
			}, 1000);
		});
	}
		
	var boxesListener = function()
	{
		if (awardNum > 0) {
			$(this).css("transform", "rotateY(0deg)");
			$(this).css("background", "white");
			$(this).css("color", "black");
			awardNum--;
		}
		else
		{
			result.fadeIn(0);
			timeout[0] = setTimeout(function(){
				result.fadeOut(0);
				theFrame.attr("data-done", "true");
				fadeNavsInAuto();
				sendCompletedStatement(1);
			}, 5000);
		}
	}
	boxes.off("click", boxesListener);
	boxes.on("click", boxesListener);
	
	fadeNavsOut();
	fadeLauncherIn();
	teacherCloud.fadeOut(0);
	result.fadeOut(0);
	teacherMouth.fadeOut(0);
	boxes.fadeOut(0);
	
	startButtonListener = function(){
		teacherMouth.fadeIn(0);
		if(awardNum === 0)
			cloud1.fadeIn(0);
		else if(awardNum < 4)
			cloud2.fadeIn(0);
		else
			cloud3.fadeIn(0);
		
		typingText[0].write();
	};
	timeout[0] = setTimeout(function(){
		startButtonListener();
		sendLaunchedStatement(25);
	}, 2000);
}

launch["frame-126"] = function()
{
	var theFrame = $("#frame-115"),
		theClone = theFrame.clone(),
		prefix = "#" + theFrame.attr("id") + " ";
		
	fadeNavsOut();
}

launch["frame-127"] = function()
{
	var theFrame = $("#frame-127"),
		theClone = theFrame.clone(),
		prefix = "#" + theFrame.attr("id") + " ",
		answer = $(prefix + ".answer");
		
	var answerListener = function(){
		$(this).css("transform", "rotateY(0deg)");
		$(this).css("color", "black");
	}
	
	answer.off("click", answerListener);
	answer.on("click", answerListener);
}

var hideEverythingBut = function(elem)
{
	if(theFrame)
		theFrame.html(theClone.html());
	
	var frames = $(".frame");
	
	frames.fadeOut(0); 
	elem.fadeIn(0);
	
	$(".award-num .label").html(awardNum);
	$(".award-num").fadeIn(0);
	
	regBox = $(".reg-box");
	fadeTimerOut();
	
	if(!globalName)
		regBox.fadeOut(0);
	
	if(elem.attr("id") === "frame-000")
	{
		fadeNavsOut();
		fadeTimerOut();
		regBox.fadeIn(0);
		$(".award-num").fadeOut(0);
	}
	else if(elem.attr("id") === "frame-105" || elem.attr("id") === "frame-108"
			|| elem.attr("id") === "frame-113")
	{
		$(".award-num").css({
			"left": "50%", 
			"top": "5%"
		});
	}
	else if(elem.attr("id") === "frame-106")
	{
		$(".award-num").css({
			"left": "80%", 
			"top": "85%"
		});
	}
	else if(elem.attr("id") === "frame-126" || elem.attr("id") === "frame-127")
		$(".award-num").fadeOut(0);
	
	for (var i = 0; i < audio.length; i++)
		audio[i].pause();	

	for (var i = 0; i < timeout.length; i++)
		clearTimeout(timeout[i]);
	
	for (var i = 0; i < typingText.length; i++)
		typingText[i].stopWriting();
	
	launch[elem.attr("id")]();
	initMenuButtons();
}

var initMenuButtons = function(){
	var links = $(".link");
	links.click(function(){
		var elem = $("#"+$(this).attr("data-link"));
		hideEverythingBut(elem);
	});
};

var main = function()
{
	var video = $(".intro-video"),
		pic = $(".intro-pic");
	
	hideEverythingBut($("#frame-000"));
	
	setMenuStuff();
	video.attr("width", video.parent().css("width"));
	video.attr("height", video.parent().css("height"));
	video[0].play();
	
	timeout[0] = setTimeout(function(){
		video.hide();
	}, 10000);
	timeout[1] = setTimeout(function(){
		pic.hide(); 
	}, 15000);
};

$(document).ready(main);